# SWITCH TO VS CODE
# napusten projekat , koristiti vs code za linux 

![spooky](/img/sp.jpeg)


#Atom "IDE" editor configuration files V 1.1
- Full stack web development packages (ES6,Angular,PHP,Python,emmet,linters ...)

**Tested on:**
- Ubuntu 14.04 LTS VM

**Contains:**
- packages.lista text file


[1] **Requirements:**
- Installed Atom editor with apm

[2] **Installation:**
- download packages.lista
- run command:$ `apm install --packages-file packages.lista`
- this will install all packages from packages.lista file
- **after installation update packages**
- in Atom editor menu go to `Packages`->`Settings View`->`Open`->`Check for Updates`->`Update all`

[3] **Packages list:**
- angularjs@0.3.4
- angularjs-snippets@0.10.18
- angularjs-styleguide-snippets@0.7.3
- atom-beautify@0.29.7
- atom-css-class-checker@0.4.3
- atom-qunit-snippets@1.1.0
- atom-svg-icon-snippets@0.3.1
- autocomplete-python@1.7.2
- color-picker@2.1.1
- csslint@1.1.4
- emmet@2.4.3
- es6-javascript@0.7.0
- file-icons@1.7.8
- git-time-machine@1.5.1
- jslint@1.5.1
 -json-schema@0.1.15
 -linter@1.11.4
- linter-jshint@2.1.0
- linter-jsonlint@1.2.5
- linter-pep8@1.3.0
- linter-php@1.2.0
- linter-python-pep8@0.2.0
- minimap@4.23.2
- pigments@0.26.0
